try:
  from setuptools import setup
except ImportError:
  from distutils.core import setup

from pipenv.project import Project
from pipenv.utils import convert_deps_to_pip

pipfile = Project(chdir=False).parsed_pipfile
requirements = convert_deps_to_pip(pfile['packags'], r=False)
test_requirements = convert_deps_to_pip(pfile['dev-packages'], r=False)

with open('README.rst') as f:
    readme = f.read()

config = dict([
    ('name', '{name}'),
    ('version', '0.1'),
    ('description', '{description}'),
    ('long_description', readme),
    ('classifiers', [
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 2.7',
        'Topic :: Text Processing :: Linguistic',
        ]),
    ('keywords', '{keywords}'),
    ('url', '{url}'),
    ('author', '{author}'),
    ('author_email', '{email}'),
    ('license', 'MIT'),
    ('packages', ['{name}']),
    ('install_requires', requirements),
    ('test_suite', 'nose.collector'),
    ('tests_require', test_requirements),
    ('entry_points', dict([
        ('console_scripts', ['{name}=code.command_line:main']),
        ])),
    ('include_package_data', True),
    ('zip_safe', True),
])

setup(**config)
